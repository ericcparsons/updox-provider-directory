# Updox Providers Directory

### Live Demo: https://parsons-updox-provider-directory.vercel.app/

#### Author: Eric Parsons

#### Preqrequisites:

    node: v16.8.0
    npm: v7.21.0

Node.js & npm can be downloaded here: https://nodejs.org/en/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) TS template.

This project also leverages

- Bootstrap and Flexbox for additional styling.
- Jest and the React Testing Library for tests.
- React-Router for SPA routes.

To run:

```
cd updox-provider-directory
npm i
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\

To test:

```
cd updox-provider-directory
npm i
npm test
```

Launches the test runner in the interactive watch mode.

## Additional Notes:

- This project uses JavaScript's built in `localStorage` functionality to store data persistently and act like a database. The API is implemented behaves like a database would and the user is able to perform async functions on the "database" to retrieve from `localStorage`. This is done also to utilzie Redux and using Thunks. That being said, the Provider Directory can be reset at any time to the "default" list (JSON list provided in the Technical Document), by navigating to: **`/reset`**

- While the project includes various automated tests, local manual testing was also preformed by running screnarios through the UI. These included tests such as adding/removing contacts. Filtering, then adding/removing. Sorting by all the properties (ascending/descending), and filtering and sorting as well. Additionally UI testing was done by testing the app for responsiveness on both Web and Mobile.

## Additional Features:

- Project could be expanded to include a backend with either a MySQL or MongoDB databas so that data will persit across browsers and be encrypted. Would likely build the backend server using Express.js

- Project could use some additional error handling, for the sake of the short timeline to completion the app doesn't respond currently when a query/entry/removal to the database/backend fails. In a future state, the UI could be updated to display a modal or information panel that informs the user that the backed server is down or not responding, or also include a general error message when "Something went wrong" on the front end side of things.

- Project could be updated to include authentication/authorization so users can store their own lists and view them through some sort of encryption, and also have their own account page/settings.

- More robust testing could be done with additional test cases, could even utilize something like Selenium to do more vigorous testing.
