import React, { useEffect, useState } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import '../../App.css';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { ProviderListHeader, ProviderListElement } from '../../components';
import { ProviderContact } from '../../models/interfaces';
import { getProviders, removeProviders } from '../providerList/providersSlice';
import { selectFilteredContacts } from './providersSlice';

/**
 * Main List of Providers, represents the providersSlice in the Redux Store
 * @returns
 */
export const ProviderList = () => {
  const dispatch = useAppDispatch();
  //all providers currently living in redux store
  let filteredProviders: ProviderContact[] = useAppSelector(
    selectFilteredContacts
  );
  let [itemsToRemove, setItemsToRemove] = useState<string[]>([]);

  //update redux state a retrieve all contacts anytime a contact is updated
  useEffect(() => {
    dispatch(getProviders());
  }, []);

  /**
   * keep track of all the list items that are currently checked
   * @param event
   */
  const addNewCheckedItem = (event: any) => {
    //the id of the Provider is the same as the name of the checkbox
    const id = event.target.name;
    if (event.target.checked) {
      //if the list item is checked, add it's id to current state
      setItemsToRemove([...itemsToRemove, id]);
    } else {
      //if the list item is unchecked, remove it's id from current state
      let newItemsToRemove = itemsToRemove.filter((el: string) => el !== id);
      setItemsToRemove(newItemsToRemove);
    }
  };

  return (
    <div>
      <ProviderListHeader />
      <div className='ProviderListContainer'>
        <ListGroup as='ol' className='ProviderList'>
          {filteredProviders.map((el, index) => {
            return (
              <ProviderListElement
                key={`provider${index}`}
                index={index}
                contact={el}
                addNewCheckedItem={addNewCheckedItem}
              />
            );
          })}
        </ListGroup>
      </div>
      <div className='ProviderRemoveContainer'>
        <Button
          id='ProviderRemoveButton'
          variant='danger'
          data-testid='RemoveButton'
          onClick={() => dispatch(removeProviders(itemsToRemove))}
        >
          Remove
        </Button>
      </div>
    </div>
  );
};
