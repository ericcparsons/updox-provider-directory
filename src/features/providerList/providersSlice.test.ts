import { ProviderContact, ProviderInitialState } from '../../models/interfaces';
import defaultProviders from './api/providers.json';
import providersReducer, { filterProviders } from './providersSlice';

describe('providers reducer', () => {
  const initialState: ProviderInitialState = {
    contacts: [],
    filteredContacts: [],
    status: 'idle',
    sortMethod: '',
  };
  it('should handle initial state', () => {
    expect(providersReducer(initialState, { type: 'unknown' })).toEqual({
      contacts: [],
      filteredContacts: [],
      status: 'idle',
      sortMethod: '',
    });
  });

  it('should update the filteredContacts when filtering providers', () => {
    const mockFilteredData = defaultProviders.filter(
      (el: ProviderContact) => el.first_name === 'Mike'
    );
    providersReducer(initialState, filterProviders(mockFilteredData));
    expect(
      providersReducer(initialState, filterProviders(mockFilteredData))
        .filteredContacts
    ).toEqual(mockFilteredData);
  });
});
