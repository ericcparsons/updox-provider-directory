import { ProviderApi } from './providersAPI';
import defaultProviders from './providers.json';
import { ProviderContact } from '../../../models/interfaces';

describe('Provider API', () => {
  it('it should initially return an empty array from local storage', async () => {
    const result = await ProviderApi.getInstance().getAll();
    expect(result).toEqual([]);
  });

  it('should add the given provider to local storage, and return an array containing that same provider', async () => {
    const result =
      (await ProviderApi.getInstance().putProvider(defaultProviders[0])) || [];
    //item should be in return value
    expect(result[0]).toEqual(defaultProviders[0]);
    //return value should be the same as what's in localStorage (aka the DB)
    expect(result).toEqual(
      JSON.parse(localStorage.getItem('providers') as string)
    );
  });

  it('should remove two of the providers from local storage (DB) given their ids and return any remaining providers', async () => {
    //fill the local storage with our mock data
    localStorage.setItem('providers', JSON.stringify(defaultProviders));

    //get an array containing two of the ids
    const ids = defaultProviders
      .map((el: ProviderContact) => el._id)
      .slice(0, 1);
    //remove the two
    await ProviderApi.getInstance().removeProviders(ids);

    //get what's in local storage (from the db);
    const persistentProviders = JSON.parse(
      localStorage.getItem('providers') as string
    );

    //one should be left
    expect(persistentProviders.length).toEqual(2);
    //should equal the first element in the default array only
    expect(persistentProviders[0]).toEqual(defaultProviders[1]);
  });

  it('should reset the providers in local storage with the contents in default array', async () => {
    const result = (await ProviderApi.getInstance().resetProviders()) || [];
    const persistentProviders = JSON.parse(
      localStorage.getItem('providers') as string
    );
    //DB should contain exactly what the mock default providers are
    expect(persistentProviders.length).toEqual(defaultProviders.length);
    expect(persistentProviders).toEqual(defaultProviders);
    expect(persistentProviders.length).toEqual(result.length);
    expect(persistentProviders).toEqual(result);
  });

  it('should give back a sorted array based on a provided key', async () => {
    const mock_fn_asc = [
      { first_name: 'A' },
      { first_name: 'B' },
      { first_name: 'C' },
    ];
    const mock_fn_desc = [
      { first_name: 'C' },
      { first_name: 'B' },
      { first_name: 'A' },
    ];

    //get what's in the localStorage (DB)
    localStorage.setItem('providers', JSON.stringify(mock_fn_asc));
    //sort by ascending, first_name
    const result = await ProviderApi.getInstance().sortProviders(
      'first_name',
      true
    );
    expect(result).toEqual(mock_fn_asc);
    //sort by descending, first_name
    const result2 = await ProviderApi.getInstance().sortProviders(
      'first_name',
      false
    );
    expect(result2).toEqual(mock_fn_desc);
  });

  it('should give back a sorted array that sorts with numbers', async () => {
    const mock_fn_asc2 = [
      { first_name: '0' },
      { first_name: '1' },
      { first_name: '2' },
    ];
    const mock_fn_desc2 = [
      { first_name: '2' },
      { first_name: '1' },
      { first_name: '0' },
    ];
    localStorage.setItem('providers', JSON.stringify(mock_fn_asc2));
    const result3 = await ProviderApi.getInstance().sortProviders(
      'first_name',
      true
    );
    expect(result3).toEqual(mock_fn_asc2);
    const result4 = await ProviderApi.getInstance().sortProviders(
      'first_name',
      false
    );
    expect(result4).toEqual(mock_fn_desc2);
  });

  it('throws when given an invalid key', async () => {
    const mock_fn_asc2 = [
      { first_name: '0' },
      { first_name: '1' },
      { first_name: '2' },
    ];
    localStorage.setItem('providers', JSON.stringify(mock_fn_asc2));
    expect(
      async () =>
        await ProviderApi.getInstance().sortProviders('last_name', true)
    ).rejects.toThrow();
  });
});

export {};
