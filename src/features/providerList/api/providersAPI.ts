import { ProviderContact } from '../../../models/interfaces';
import defaultProviders from './providers.json';

/**
 * Singleton class for any writes or reads to the persistent storage of Providers in the Providers Directory
 * Mocks and behaves like a real-world database would by delaying any "calls" to the DB, requiring queries to be run asynchronously
 */
export class ProviderApi {
  private static instance: ProviderApi;
  private storageKey = 'providers';

  private constructor() {}

  // A mock function to mimic making an async request to a server or external API for data
  private delay = (delay = 0) =>
    new Promise((resolve) => setTimeout(resolve, delay));

  /**
   * retrieve from "DB" aka localStorage
   * @param val
   * @returns
   */
  private getFromLocalStorage = (val: string) => {
    return localStorage.getItem(val);
  };

  /**
   * add value to "DB" aka localStorage
   * @param key
   * @param val
   * @param delay
   * @returns
   */
  private setLocalStorage = (key: string, val: string) => {
    localStorage.setItem(key, val);
  };

  /**
   * getAll of the Providers Listen from DB
   * @returns [] of ProviderContacts
   */
  public async getAll(): Promise<ProviderContact[]> {
    return this.delay(1000)
      .then(() => {
        const local = this.getFromLocalStorage(this.storageKey);
        let storedProviders = JSON.parse(local as string) || [];
        return storedProviders;
      })
      .catch((e) => {
        if (e instanceof Error)
          throw new Error(
            `Unable to get all Providers from DB => ${e.message}`
          );
      });
  }

  /**
   * Put (add) a single Provider Contact into the list in the DB
   * @param contact: ProviderContact
   * @returns [] of ProviderContacts with added Contact
   */
  public async putProvider(
    contact: ProviderContact
  ): Promise<ProviderContact[]> {
    return this.delay(1000)
      .then(() => {
        const local = this.getFromLocalStorage(this.storageKey);
        const currentProviders = JSON.parse(local as string) || [];
        const newProviders: ProviderContact[] = [...currentProviders, contact];
        this.setLocalStorage(this.storageKey, JSON.stringify(newProviders));
        return newProviders;
      })
      .catch((e) => {
        throw new Error(`Failed to add a Provider => ${e.message}`);
      });
  }

  /**
   * Given an array of unique contact ids, remove all of those from the DB
   * @param idsToRemove
   */
  public async removeProviders(idsToRemove: string[]) {
    for (let id of idsToRemove) {
      await this.removeProvider(id).catch((e) => {
        if (e instanceof Error)
          throw new Error(
            `Unable to remove list of providers from DB => ${e.message}`
          );
      });
    }
  }

  /**
   * Given a single unique contact id, remove the contact from the DB list
   * @param id
   * @returns
   */
  public async removeProvider(id: string) {
    return this.delay(100)
      .then(() => {
        const local = this.getFromLocalStorage(this.storageKey);
        const currentProviders = JSON.parse(local as string);
        const newProviders = currentProviders.filter(
          (el: ProviderContact) => el._id !== id
        );
        this.setLocalStorage(this.storageKey, JSON.stringify(newProviders));
      })
      .catch((e) => {
        if (e instanceof Error)
          throw new Error(
            `Unable to remove individual provider => ${e.message}`
          );
      });
  }

  /**
   * Debug feature: reset the DB to contain only the default values
   * @returns [] containing the default values from JSON
   */
  public async resetProviders() {
    const res = this.delay()
      .then(() => {
        this.setLocalStorage(this.storageKey, JSON.stringify(defaultProviders));
        return defaultProviders;
      })
      .catch((e) => {
        if (e instanceof Error)
          throw new Error(
            `Unable to reset providers to default list => ${e.message} `
          );
      });
    return res;
  }

  /**
   * give back whatever is in the list, but sorted based on the given key and ascending value
   * @param keyToSortBy : ProviderContact key to sort by i.e. "first_name"
   * @param ascending : true if sort by ascending, false if sort by descending
   * @returns
   */
  public sortProviders(keyToSortBy: string, ascending: boolean) {
    let currentProviders = JSON.parse(
      this.getFromLocalStorage(this.storageKey) as string
    );
    try {
      let sortedProviders = ascending
        ? currentProviders.sort(
            (a: { [key: string]: string }, b: { [key: string]: string }) =>
              a[keyToSortBy].localeCompare(b[keyToSortBy])
          )
        : currentProviders.sort(
            (a: { [key: string]: string }, b: { [key: string]: string }) =>
              b[keyToSortBy].localeCompare(a[keyToSortBy])
          );
      return sortedProviders;
    } catch (e) {
      if (e instanceof Error) {
        throw new Error(
          `Error when sorting providers, likely a bad key given, check inputs: ${e.message}`
        );
      }
    }
  }

  /**
   * Singleton, if an instance doesn't exist, create it
   * @returns ProviderAPI Instance
   */
  public static getInstance() {
    if (!(this.instance instanceof ProviderApi)) {
      this.instance = new ProviderApi();
    }
    return this.instance;
  }
}
