import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import { configureTestStore } from '../../tests/helpers';
import { ProviderList } from './ProviderList';

test('should load and render buttons and header', async () => {
  const store = configureTestStore();
  render(
    <Provider store={store}>
      <ProviderList />
    </Provider>
  );
  // should show buttons and header in beginning
  expect(screen.getByText(/Provider List/i)).toBeInTheDocument();
  expect(screen.getByText(/Remove/i)).toBeInTheDocument();
  expect(screen.getByText(/Sort By/i)).toBeInTheDocument();

  //initial contacts should be in the list
  expect(screen.getByText(/Harris, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Carlson, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Juday, Tobin*/i)).toBeInTheDocument();
});

test('should load the defaultProviders on screen', async () => {
  const store = configureTestStore();
  render(
    <Provider store={store}>
      <ProviderList />
    </Provider>
  );
  //initial contacts should be in the list
  expect(screen.getByText(/Harris, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Carlson, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Juday, Tobin*/i)).toBeInTheDocument();
});

test('should remove providers from the list when selected', async () => {
  const store = configureTestStore();
  render(
    <Provider store={store}>
      <ProviderList />
    </Provider>
  );
  //initial contacts should be in the list
  let mikeHarris = screen.queryByTestId(
    /8c3cdca9-2184-4251-a24d-bcdd60990b3e*/i
  ) as HTMLElement;
  const removeButton = screen.getByTestId(/RemoveButton*/i) as any;

  //select the list element, and click remove
  expect(mikeHarris).toBeInTheDocument();
  fireEvent.change(mikeHarris, { target: { checked: true } });
  fireEvent.click(removeButton);
  await waitForElementToBeRemoved(mikeHarris);

  //element should be gone
  expect(mikeHarris).not.toBeVisible();
});
