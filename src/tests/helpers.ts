import { configureStore } from '@reduxjs/toolkit';
import providersSlice from '../features/providerList/providersSlice';
import defaultProviders from '../features/providerList/api/providers.json';

export const initialState = {
  providers: {
    contacts: defaultProviders,
    filteredContacts: defaultProviders,
    status: 'idle',
    sortMethod: '',
  },
};

export function configureTestStore() {
  return configureStore({
    reducer: {
      providers: providersSlice,
    },
    preloadedState: initialState as any,
  });
}
