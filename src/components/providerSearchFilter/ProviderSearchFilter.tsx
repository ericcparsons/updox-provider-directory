import React, { useEffect, useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {
  filterProviders,
  selectAllContacts,
} from '../../features/providerList/providersSlice';
import { ProviderContact } from '../../models/interfaces';

/**
 * Search Bar in Provider List and all it's functionality
 * @returns
 */
export const ProviderSearchFilter = () => {
  const dispatch = useAppDispatch();
  //hold the value currently in the search bar in local state
  const [searchFilter, setSearchFilter] = useState('');
  //get all of the providers from db/persistent data
  let providers: ProviderContact[] = useAppSelector(selectAllContacts);

  /**
   * handle any changes in the search form
   * @param e
   */
  const handleChange = (e: any) => {
    setSearchFilter(e.target.value);
  };

  /**
   * update the filteredContacts list in the redux store so that the list displays a filtered view
   */
  const sendFilteredListToStore = () => {
    //if something is in the search bar, filter the provdiers list and send to redux store
    if (searchFilter !== '') {
      const filtered = providers.filter((el: ProviderContact) =>
        Object.values(el)
          .join(' ')
          .toLowerCase()
          .includes(searchFilter.toLowerCase())
      );
      dispatch(filterProviders(filtered));
    } else {
      //just show the providers
      dispatch(filterProviders(providers));
    }
  };

  //update the filtered array anytime the search/filter is updated, or anytime a provider is created/removed
  useEffect(() => {
    sendFilteredListToStore();
  }, [searchFilter, providers]);

  return (
    <InputGroup className='mb-3' id='ProviderSearchBar'>
      <FormControl
        aria-label='Default'
        aria-describedby='inputGroup-sizing-default'
        placeholder='Search Directory'
        onChange={handleChange}
      />
    </InputGroup>
  );
};
