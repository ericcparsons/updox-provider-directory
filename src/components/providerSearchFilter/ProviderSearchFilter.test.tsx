import { act, fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ProviderList } from '../../features/providerList/ProviderList';
import { configureTestStore } from '../../tests/helpers';

test('should not display anything in list when a non-applicable filter is in the search bar', async () => {
  const store = configureTestStore();
  const {} = render(
    <Provider store={store}>
      <ProviderList />
    </Provider>
  );
  //initial contacts should be in the list (no filter specified should show everything)
  expect(screen.getByText(/Harris, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Carlson, Mike*/i)).toBeInTheDocument();
  expect(screen.getByText(/Juday, Tobin*/i)).toBeInTheDocument();

  //search something
  const searchBar = screen.getByPlaceholderText(/Search Directory/i);
  fireEvent.change(searchBar, { target: { value: 'Test' } });

  //all of the contacts should dissapear from screen since they don't apply
  expect(screen.queryByText(/Harris, Mike*/i)).toBe(null);
  expect(screen.queryByText(/Carlson, Mike*/i)).toBe(null);
  expect(screen.queryByText(/Juday, Tobin*/i)).toBe(null);
});

test('should display some values on screen based on a applicable filter in the search bar', async () => {
  const store = configureTestStore();
  const {} = render(
    <Provider store={store}>
      <ProviderList />
    </Provider>
  );
  //search something
  const searchBar = screen.getByPlaceholderText(/Search Directory/i);
  fireEvent.change(searchBar, { target: { value: 'Mike' } });

  //all of the contacts should dissapear from screen since that don't apply
  expect(screen.queryByText(/Harris, Mike*/i)).not.toBe(null);
  expect(screen.queryByText(/Carlson, Mike*/i)).not.toBe(null);
  expect(screen.queryByText(/Juday, Tobin*/i)).toBe(null);
});
