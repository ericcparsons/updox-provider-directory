import React from 'react';
import { Spinner } from 'react-bootstrap';
import '../../App.css';
import { useAppSelector } from '../../app/hooks';
import { selectLoadingStatus } from '../../features/providerList/providersSlice';
import { ProviderSearchFilter, SortMethod } from '../';

/**
 * Header row in Provider List, contains the Header text, search input and sort menu
 */
export const ProviderListHeader = () => {
  const loading = useAppSelector(selectLoadingStatus);

  return (
    <div className='ProviderListHeader'>
      <div className='d-flex'>
        <h3>Provider List</h3>
        {/* If app is loading in state, show the Bootstrap spinner */}
        {loading === 'loading' ? (
          <Spinner
            data-testid='Spinner'
            id='ProviderListSpinner'
            animation='border'
            size='sm'
          />
        ) : (
          <></>
        )}
      </div>
      <div className='ProviderSearchSortContainer'>
        <ProviderSearchFilter />
        <SortMethod />
      </div>
    </div>
  );
};
