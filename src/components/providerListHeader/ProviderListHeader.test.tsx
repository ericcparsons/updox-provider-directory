import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import { ProviderListHeader } from '../';

test('should load and render header', async () => {
  render(
    <Provider store={store}>
      <ProviderListHeader />
    </Provider>
  );
  const providerListHeader = screen.getByText(/Provider List*/i);
  expect(providerListHeader).toBeInTheDocument();
});
