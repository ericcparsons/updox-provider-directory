import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Container, Row } from 'react-bootstrap';
import '../../App.css';
import { ProviderList } from '../../features/providerList/ProviderList';
import { CreateProvider, ShadowCard } from '../';

/**
 * Main Container of all things Provider Directory (i.e. home page)
 */
export const ProviderDirectory = () => {
  return (
    <div>
      <div className='ProviderDirectoryHeader'>
        <h1>Provider Directory</h1>
      </div>
      <Container fluid className='ProviderDirectoryContainer'>
        <Row className='ShadowCardRow'>
          <ShadowCard colWidth={4}>
            <CreateProvider />
          </ShadowCard>
          <ShadowCard colWidth={8}>
            <ProviderList />
          </ShadowCard>
        </Row>
      </Container>
    </div>
  );
};
