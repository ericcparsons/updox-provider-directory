import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import { ProviderDirectory } from '../';

test('should load and render main header', async () => {
  render(
    <Provider store={store}>
      <ProviderDirectory />
    </Provider>
  );
  const providerDirectoryHeader = screen.getByText(
    /Provider Directory*/i
  ) as any;
  expect(providerDirectoryHeader).toBeInTheDocument();
});
