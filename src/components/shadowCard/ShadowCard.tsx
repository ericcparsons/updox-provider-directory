import React from 'react';
import { Col } from 'react-bootstrap';
import '../../App.css';

interface ShadowCardProps {
  children: React.ReactNode;
  colWidth: number;
}
/**
 * ShadowCard wrapper for consistent styling
 * @param colWidth: number representing how many bootstrap columns the ShadowCard will take up
 * @returns
 */
export const ShadowCard = ({ children, colWidth }: ShadowCardProps) => {
  return (
    <Col lg={colWidth} className='ShadowCardCol'>
      <div className='ShadowCard'>{children}</div>
    </Col>
  );
};
