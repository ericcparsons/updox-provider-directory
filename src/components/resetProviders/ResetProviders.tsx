import React, { useEffect } from 'react';
import { Navigate } from 'react-router';
import { useAppDispatch } from '../../app/hooks';
import { resetProviders } from '../../features/providerList/providersSlice';

/**
 * Debugging/testing feature, allows user to go to '/reset' and repopulate the list with the provided default JSON values
 * @returns
 */
export const ResetProviders = () => {
  const dispatch = useAppDispatch();

  //on load, reset the providers list to default
  useEffect(() => {
    dispatch(resetProviders());
  }, []);
  //redirect back to home
  return <Navigate to='/' />;
};
