import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ProviderListElement } from '../';
import defaultProviders from '../../features/providerList/api/providers.json';
import { configureTestStore } from '../../tests/helpers';

test('should load a list item that displays the given props contact information', async () => {
  const store = configureTestStore();
  render(
    <Provider store={store}>
      <ProviderListElement
        index={0}
        contact={defaultProviders[0]}
        addNewCheckedItem={() => {}}
      />
    </Provider>
  );
  const name = screen.getByText(/Harris, Mike*/i);
  const email = screen.getByText(/mharris@updox.com*/i);

  //assert the contact name and email are on the screen
  waitFor(() => expect(name).toBeInTheDocument());
  waitFor(() => expect(email).toBeInTheDocument());
});
