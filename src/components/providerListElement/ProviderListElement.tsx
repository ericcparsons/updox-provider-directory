import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { ProviderContact } from '../../models/interfaces';

interface ProviderListItemProps {
  index: number;
  contact: ProviderContact;
  addNewCheckedItem: any;
}

/**
 * A single row in the Provider Directory List
 * @params {
 * index: index the current list item is in the upper list of ProviderListItem
 * contact: contact information passed down
 * addNewCheckedItem: callback to update state of upper list for which items are "checked" or "selected"
 * }
 */
export const ProviderListElement = ({
  index,
  contact,
  addNewCheckedItem,
}: ProviderListItemProps) => {
  return (
    <ListGroup.Item
      variant={index % 2 === 0 ? 'secondary' : 'light'}
      as='li'
      className='ListGroupItem d-flex'
    >
      <div className='form-check'>
        <input
          type='checkbox'
          id='ListGroupCheckbox'
          data-testid={`${contact._id}`}
          name={`${contact._id}`}
          className='form-check-input'
          onChange={(e: any) => addNewCheckedItem(e)}
        />
      </div>
      <div className='ProviderListItemLeft ms-2 me-auto'>
        <div className='fw-bold'>{`${contact.last_name}, ${contact.first_name}`}</div>
        {`${contact.email_address}`}
      </div>
      <div className='ProviderListItemRight'>
        <div>{`${contact.specialty}`}</div>
        <div>{`${contact.practice_name}`}</div>
      </div>
    </ListGroup.Item>
  );
};
