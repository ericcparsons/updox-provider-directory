import React from 'react';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import { useAppDispatch } from '../../app/hooks';
import { updateSort } from '../../features/providerList/providersSlice';
import { SortMethodObj } from '../../models/interfaces';

/**
 * Sort Method dropdown for Provider List
 */
export const SortMethod = () => {
  const dispatch = useAppDispatch();

  /**
   * Handle any new slections of sort methods and update the visible sort method in the Redux Store
   * @param e
   */
  const handleSelect = (e: any) => {
    const sortMethod: SortMethodObj = {
      //Method matches the keys adhering to ProviderContact
      method: e.split('-')[0],
      ascending: eval(e.split('-')[1]),
    };
    dispatch(updateSort(sortMethod));
  };

  return (
    <DropdownButton
      onSelect={(e) => handleSelect(e)}
      id='dropdown-basic-button'
      title='Sort By'
    >
      <Dropdown.Item eventKey='last_name-true'>Last Name(A-Z)</Dropdown.Item>
      <Dropdown.Item eventKey='last_name-false'>Last Name(Z-A)</Dropdown.Item>
      <Dropdown.Item eventKey='first_name-true'>First Name(A-Z)</Dropdown.Item>
      <Dropdown.Item eventKey='first_name-false'>First Name(Z-A)</Dropdown.Item>
      <Dropdown.Item eventKey='email_address-true'>Email(A-Z)</Dropdown.Item>
      <Dropdown.Item eventKey='email_address-false'>Email(Z-A)</Dropdown.Item>
      <Dropdown.Item eventKey='specialty-true'>Specialty(A-Z)</Dropdown.Item>
      <Dropdown.Item eventKey='specialty-false'>Specialty(Z-A)</Dropdown.Item>
      <Dropdown.Item eventKey='practice_name-true'>Practice(A-Z)</Dropdown.Item>
      <Dropdown.Item eventKey='practice_name-false'>
        Practice(Z-A)
      </Dropdown.Item>
    </DropdownButton>
  );
};
