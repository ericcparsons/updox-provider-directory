import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import '../../App.css';
import { useAppDispatch } from '../../app/hooks';
import { addProvider } from '../../features/providerList/providersSlice';
import { ProviderContact } from '../../models/interfaces';

//default value for currentPoll state
const emptyProvider: ProviderContact = {
  _id: '',
  last_name: '',
  first_name: '',
  email_address: '',
  specialty: '',
  practice_name: '',
};

/**
 * Component for Create Provider form on homepage
 * @returns
 */
export const CreateProvider = () => {
  const dispatch = useAppDispatch();
  const [newProvider, setNewProvider] =
    useState<ProviderContact>(emptyProvider);
  const [validated, setValidated] = useState(false);

  /**
   * handle submit and add new provider to the redux store if the form meets all validation requirements
   * @param event
   */
  const handleSubmit = (event: any) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault(); //prevent screen from reloading on form submit
      event.stopPropagation();
      setValidated(true);
    } else {
      const newId = uuidv4(); //give new provider a unique id
      const provider = { ...newProvider, _id: newId };
      event.preventDefault();
      dispatch(addProvider(provider));
      setNewProvider(emptyProvider);
      setValidated(false);
    }
  };

  /**
   * handle any changes and update local 'newProvider' state to be submitted and pushed to redux store on submit
   * @param event: DOM event
   */
  let handleChange = (event: any) => {
    setNewProvider({ ...newProvider, [event.target.name]: event.target.value });
  };

  return (
    <div className='CreateProviderContainer'>
      <h3>Create Provider</h3>
      <Form
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
        className='CreateProviderForm'
      >
        <Form.Group>
          <Form.Label id='my-test-id'>Last Name*</Form.Label>
          <Form.Control
            required
            onChange={(e) => handleChange(e)}
            name='last_name'
            placeholder='Last Name'
            value={newProvider.last_name}
          />
          <Form.Control.Feedback type='invalid'>
            Please enter a last name.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group>
          <Form.Label>First Name*</Form.Label>
          <Form.Control
            required
            onChange={(e) => handleChange(e)}
            name='first_name'
            placeholder='First Name'
            value={newProvider.first_name}
          />
          <Form.Control.Feedback type='invalid'>
            Please enter a first name.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group>
          <Form.Label>Email Address*</Form.Label>
          <Form.Control
            required
            onChange={(e) => handleChange(e)}
            name='email_address'
            placeholder='Enter Email'
            value={newProvider.email_address}
          />
          <Form.Control.Feedback type='invalid'>
            Please enter an email address.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group>
          <Form.Label>Specialty</Form.Label>
          <Form.Control
            onChange={(e) => handleChange(e)}
            name='specialty'
            placeholder='Enter Specialty'
            value={newProvider.specialty}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Practice Name</Form.Label>
          <Form.Control
            onChange={(e) => handleChange(e)}
            name='practice_name'
            placeholder='Practice Name'
            value={newProvider.practice_name}
          />
        </Form.Group>
        <Button variant='primary' type='submit' data-testid='SubmitButton'>
          Submit
        </Button>
      </Form>
    </div>
  );
};
