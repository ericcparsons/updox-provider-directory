import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import { ProviderList } from '../../features/providerList/ProviderList';
import { configureTestStore } from '../../tests/helpers';
import { CreateProvider } from './CreateProvider';

test('should load and render buttons and form', async () => {
  render(
    <Provider store={store}>
      <CreateProvider />
    </Provider>
  );
  const submitButton = screen.getByTestId(/SubmitButton*/i) as any;
  const lastNameField = screen.getByText(/Last Name\*/i) as any;
  expect(submitButton).toBeInTheDocument();
  expect(lastNameField).toBeInTheDocument();
});

test('should warn when required fields are not populated', async () => {
  render(
    <Provider store={store}>
      <CreateProvider />
    </Provider>
  );
  const submitButton = screen.getByTestId(/SubmitButton*/i) as any;
  fireEvent.click(submitButton);
  expect(screen.queryAllByText(/Please enter a/i)).toHaveLength(3);
});

test('should add new provider to list, and not warn when required fields are populated', async () => {
  const store = configureTestStore();

  render(
    <Provider store={store}>
      <CreateProvider />
      <ProviderList />
    </Provider>
  );
  const lastNameField = screen.getByPlaceholderText(/Last Name/i) as any;
  fireEvent.change(lastNameField, { target: { value: 'Test' } });
  const firstNameField = screen.getByPlaceholderText(/First Name/i) as any;
  fireEvent.change(firstNameField, { target: { value: 'Test' } });
  const emailField = screen.getByPlaceholderText(/Enter Email/i) as any;
  fireEvent.change(emailField, { target: { value: 'Test' } });
  const submitButton = screen.getByTestId(/SubmitButton*/i) as any;
  fireEvent.click(submitButton);
  await waitFor(() => expect(screen.queryByText(/Test, Test/i)).toBeVisible());
});
