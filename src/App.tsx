import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import { ProviderDirectory, ResetProviders } from './components';

function App() {
  return (
    <BrowserRouter>
      <div className='App'>
        <Routes>
          <Route index element={<ProviderDirectory />} />
          <Route path='/reset' element={<ResetProviders />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
