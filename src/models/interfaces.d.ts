export interface ProviderContact {
  _id: string;
  last_name: string;
  first_name: string;
  email_address: string;
  specialty: string;
  practice_name: string;
}

export interface ProviderInitialState {
  contacts: ProviderContact[];
  filteredContacts: ProviderContact[];
  status: 'idle' | 'failed' | 'loading';
  sortMethod: string;
}

export interface SortMethodObj {
  method: string;
  ascending: boolean;
}
